
# Learn how to

https://developer.chrome.com/extensions/getstarted



# Create `manifest.json`

```json
{
    "name": "Baldir.fr extension",
    "version": "1.0",
    "description": "Extension that plugs into baldir.fr apps",
    "manifest_version": 2
}
```

# Try to load in chrome developer mode

* In your chrome compatible browser : `chrome://extensions`
* Activate dev mode if not already
* Load unpacked
* Open the path or the folder where you created the `manifest.json` file

![](https://developer.chrome.com/static/images/get_started/load_extension.png)

# Add a background script

```json
{
    "name": "Baldir.fr extension",
    "version": "1.0",
    "description": "Extension that plugs into baldir.fr apps",
    "background": {
      "scripts": ["background.js"],
      "persistent": false
    },
    "manifest_version": 2
}
```

# Listen for an event from the background script

`background.json` 

```js
chrome.runtime.onInstalled.addListener(function () {
    chrome.storage.sync.set({ color: '#3aa757' }, function () {
        console.log("The color is green.");
    });
});
```

This will listen to the event triggered when the extension is installed.

Test it by reloading the extension in `chrome://extensions` you can also check
console in "Inspect views background page" link.

![](https://developer.chrome.com/static/images/get_started/view_background.png)

# Add an html view

`popup.html`

```html
<!DOCTYPE html>
  <html>
    <head>
      <style>
        button {
          height: 30px;
          width: 30px;
          outline: none;
        }
      </style>
    </head>
    <body>
      <button id="changeColor"></button>
    </body>
  </html>
```

# Add the view to manifest

`manifest.json`

```json
{
    "name": "Baldir.fr extension",
    "version": "1.0",
    "description": "Extension that plugs into baldir.fr apps",
    "permissions": [
        "storage"
    ],
    "background": {
        "scripts": [
            "background.js"
        ],
        "persistent": false
    },
    "page_action": {
        "default_popup": "popup.html"
    },
    "manifest_version": 2
}
```

# Add icon to the extension toolbar

Add a folder with icons in several sizes. Then reference them in the manifest.

`manifest.json` 

```json
{
    "name": "Baldir.fr extension",
    "version": "1.0",
    "description": "Extension that plugs into baldir.fr apps",
    "permissions": [
        "storage"
    ],
    "background": {
        "scripts": [
            "background.js"
        ],
        "persistent": false
    },
    "page_action": {
        "default_popup": "popup.html",
        "default_icon": {
            "16": "images/get_started16.png",
            "32": "images/get_started32.png",
            "48": "images/get_started48.png",
            "128": "images/get_started128.png"
        }
    },
    "manifest_version": 2
}
```



# Add icon for the management page

Add a folder with icons in several sizes. Then reference them in the manifest.

`manifest.json` 

```json
{
    "name": "Baldir.fr extension",
    "version": "1.0",
    "description": "Extension that plugs into baldir.fr apps",
    "permissions": [
        "storage"
    ],
    "background": {
        "scripts": [
            "background.js"
        ],
        "persistent": false
    },
    "page_action": {
        "default_popup": "popup.html",
        "default_icon": {
            "16": "images/get_started16.png",
            "32": "images/get_started32.png",
            "48": "images/get_started48.png",
            "128": "images/get_started128.png"
        }
    },
    "icons": {
      "16": "images/get_started16.png",
      "32": "images/get_started32.png",
      "48": "images/get_started48.png",
      "128": "images/get_started128.png"
    },
    "manifest_version": 2
}
```

# Enable interaction with html page

We want the page to be shown when the current host of the browser's page is `developer.chrome.com`

`background.js` 

```js
chrome.runtime.onInstalled.addListener(function () {
    chrome.storage.sync.set({ color: '#3aa757' }, function () {
        console.log("The color is green.");
    });

    chrome.declarativeContent.onPageChanged.removeRules(undefined, function () {
        chrome.declarativeContent.onPageChanged.addRules([{
            conditions: [new chrome.declarativeContent.PageStateMatcher({
                pageUrl: { hostEquals: 'developer.chrome.com' },
            })
            ],
            actions: [new chrome.declarativeContent.ShowPageAction()]
        }]);
    });
});
```

And add a new permission in the manifest.

`manifest.json`

```json
  {
    "name": "Baldir.fr extension",
  ...
    "permissions": ["declarativeContent", "storage"],
  ...
  }
```

Now open https://developer.chrome.com the extension is activated and when you click on the icon 
the popup is visible.

![](docs/making-of/open-on-developer.chrome.com.png)

# Modify the button from javascript

Create a js file containing the behaviour.
It will change background of the button.

`popup.js`

```js
  let changeColor = document.getElementById('changeColor');

  chrome.storage.sync.get('color', function(data) {
    changeColor.style.backgroundColor = data.color;
    changeColor.setAttribute('value', data.color);
  });
```

Reference it in the view.

`popup.html`

```html
<!DOCTYPE html>
<html>
...
  <body>
    <button id="changeColor"></button>
    <script src="popup.js"></script>
  </body>
</html>
```

# Add behaviour to the button

This behaviour will change the background color of the current page to green.

`popup.js`

```js
let changeColor = document.getElementById('changeColor');
  ...
  changeColor.onclick = function(element) {
    let color = element.target.value;
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
      chrome.tabs.executeScript(
          tabs[0].id,
          {code: 'document.body.style.backgroundColor = "' + color + '";'});
    });
  };
```

# Add options to the extension

`options.html`

```html
<!DOCTYPE html>
<html>

<head>
    <style>
        button {
            height: 30px;
            width: 30px;
            outline: none;
            margin: 10px;
        }
    </style>
</head>

<body>
    <div id="buttonDiv">
    </div>
    <div>
        <p>Choose a different background color!</p>
    </div>
</body>
<script src="options.js"></script>

</html>
```

`manifest.json`

```json
  {
    "name": "Baldir.fr extension",
    ...
    "options_page": "options.html",
    ...
    "manifest_version": 2
  }
```

`options.js`

```js
  let page = document.getElementById('buttonDiv');
  const kButtonColors = ['#3aa757', '#e8453c', '#f9bb2d', '#4688f1'];
  function constructOptions(kButtonColors) {
    for (let item of kButtonColors) {
      let button = document.createElement('button');
      button.style.backgroundColor = item;
      button.addEventListener('click', function() {
        chrome.storage.sync.set({color: item}, function() {
          console.log('color is ' + item);
        })
      });
      page.appendChild(button);
    }
  }
  constructOptions(kButtonColors);
  ```



# Learn more about chrome extensions

  * [Chrome extensions overview](https://developer.chrome.com/extensions/overview)

# Filter on any site

`background.js`

```js
chrome.runtime.onInstalled.addListener(function () {

    chrome.declarativeContent.onPageChanged.removeRules(undefined, function () {
        
        chrome.declarativeContent.onPageChanged.addRules([{
            conditions: [new chrome.declarativeContent.PageStateMatcher({
                pageUrl: { hostContains: '.' }, // everything is matched
            })
            ],
            actions: [new chrome.declarativeContent.ShowPageAction()]
        }]);
    });
});
```

# Capture the current URL

When the button is clicked. Capture the current URL.

`popup.html`

```html
<body>
    <button id="captureUrl"></button>
    <script src="popup.js"></script>
</body>
```

`popup.js` 

```js
let captureUrl = document.getElementById('captureUrl');

captureUrl.onclick = function () {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
            console.log(tabs[0].url)
    });
};
```

# create a quick file server

```
sudo npm install http-server -g
http-server -p 8081
```

# Receive data from backend

